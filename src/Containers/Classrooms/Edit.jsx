import React, { useEffect, useState } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
/**importar spinner */
import Spinner from "../../Complements/Spinner";
/**Importar Sweetalert2 * */
import Swal from "sweetalert2";
/**Habilitar redirecciones */
import { withRouter } from "react-router-dom";
import MethodGet, { MethodPut } from '../../Config/Services';
import { useForm } from 'react-hook-form';
import FormControl from '@material-ui/core/FormControl';

import SelectBranchOffice from '../SelectOptions/SelectBranchOffice'
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
}));

const name = [{ title: 'Studio GlamOur' }];

function ClassroomEdit(props) {
	const classes = useStyles();

	//obtener el id del cliente
	const { id } = props.match.params;

	//Trabajar con el state para guardar la informacion de classroom
	const [classroom, saveClassroom] = useState({});

	const { register, handleSubmit, errors } = useForm();

	//Trabajar con el spinner
	const [cargando, spinnerCargando] = useState(false);

	//Trabajar con el state de office
	const [officeId, saveOffice] = useState({
		id_branch_office: '',
	});

	//extraer las propiedades del objeto Desctructuracion
	const { name } = classroom;

	//Funciones para almacenar el id de classroom
	const detectarCambiosOffice = (e) => {
		saveOffice({ id_branch_office: e.target.value });
	};

	//Query para consultar el cliente
	useEffect(() => {
		let url = `/class/${id}`;
		MethodGet(url)
			.then((res) => {
				saveClassroom(res.data.data);
				saveOffice({ id_branch_office: res.data.data.id_branch_office });
				spinnerCargando(true);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	//Guardar los cambios del cliente
	const onSubmit = (data, e) => {
		e.preventDefault();
		const dat = {
			...data,
			...officeId
		};
		let url = `/class/${id}`;
		MethodPut(url, dat)
			.then((res) => {
				Swal.fire({
					title: 'Salón Editado Exitosamente',
					text: res.data.message,
					icon: 'success',
					timer: 2000,
					showConfirmButton: false,
				});
				props.history.push('/Salones');
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error',
					text: error.response.data.error,
					icon: 'error',
				});
			});
	};

	//  spinner de carga
	if (!cargando) return <Spinner />;

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Salon
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="name"
								label="Nombre del Salon"
								name="name"
								autoComplete="name"
								autoFocus
								defaultValue={name}
								error={!!errors.classroom}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre del Salon es requerido',
									},
									minLength: {
										value: 1,
										message: 'Minimo 5 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: 'Unicamente carácteres alfabéticos',
									},
								})}
							/>
							<p>{errors?.classroom?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6} style={{ paddingTop: 24 }}>
							<FormControl
								variant="outlined"
								className={classes.FormControl}
								fullWidth
							>
								<SelectBranchOffice
									officeId={officeId}
									detectarCambiosOffice={detectarCambiosOffice}
								/>
							</FormControl>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}

export default withRouter(ClassroomEdit)