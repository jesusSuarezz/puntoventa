import React, { useState, useEffect } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useForm } from 'react-hook-form';

import MethodGet, { MethodPut } from '../../Config/Services';
import Swal from 'sweetalert2';
import Spinner from '../../Complements/Spinner';
import { withRouter } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';

import SelectBranchOffice from '../SelectOptions/SelectBranchOffice';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
}));

function ProductEdit(props) {
	const classes = useStyles();
	const [cargando, spinnerCargando] = useState(false);

	const { id } = props.match.params;

	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();

	// const [operator, setOperator] = useState({});
	const [product, guardarProduct] = useState({});

	//Trabajar con el state de office
	const [officeId, saveOffice] = useState({
		id_branch_office: '',
	});

	const { name, description, price, barcode } = product;

	const detectarCambiosOffice = (e) => {
		saveOffice({ id_branch_office: e.target.value });
	};

	useEffect(() => {
		let url = `/products/${id}`;

		MethodGet(url)
			.then((res) => {
				guardarProduct(res.data.data);
				saveOffice({ id_branch_office: res.data.data.id_branch_office });
				spinnerCargando(true);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	const onSubmit = (data, e) => {
		e.preventDefault();

		const dat = {
			...data,
			...officeId,
		};

		let url = `/products/${id}`; //cambiar la url completa
		MethodPut(url, dat)
			.then((res) => {
				Swal.fire({
					title: 'Producto Editado Exitosamente',
					text: res.data.message,
					icon: 'success',
					timer: 2000,
					showConfirmButton: false,
				});
				props.history.push('/Productos');
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error',
					text: error.response.data.error,
					icon: 'error',
				});
			});
	};

	//  spinner de carga
	if (!cargando) return <Spinner />;

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Producto
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6} style={{ paddingTop: 16 }}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectBranchOffice
									officeId={officeId}
									detectarCambiosOffice={detectarCambiosOffice}
								/>
							</FormControl>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="name"
								label="Nombre"
								name="name"
								autoComplete="name"
								autoFocus
								defaultValue={name}
								error={!!errors.name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre es requerido',
									},
									minLength: {
										value: 5,
										message: 'Minimo 5 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: 'Unicamente carácteres alfabéticos',
									},
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="description"
								label="Descripcion"
								name="description"
								autoComplete="name"
								autoFocus
								defaultValue={description}
								error={!!errors.description}
								inputRef={register({
									required: {
										value: true,
										message: 'La descripcion es requerido',
									},
									minLength: {
										value: 1,
										message: 'Minimo 5 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
								})}
							/>
							<p>{errors?.description?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="quantity"
								label="Cantidad"
								name="quantity"
								type="number"
								autoFocus
								error={!!errors.quantity}
								inputRef={register({
									maxLength: {
										value: 5,
										message: 'Maximo 5 digitos',
									},
								})}
							/>
							<p>{errors?.quantity?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="price"
								label="Precio venta"
								name="price"
								type="number"
								autoFocus
								defaultValue={price}
								error={!!errors.price}
								inputRef={register({
									required: {
										value: true,
										message: 'El precio venta es requerido',
									},
									maxLength: {
										value: 10,
										message: 'Maximo 10 caracteres',
									},
								})}
							/>
							<p>{errors?.price?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="barcode"
								label="Codigo de Barras"
								name="barcode"
								autoComplete="name"
								autoFocus
								defaultValue={barcode}
								error={!!errors.barcode}
								inputRef={register({
									required: {
										value: true,
										message: 'El codigo de barras es requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
								})}
							/>
							<p>{errors?.barcode?.message}</p>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}
export default withRouter(ProductEdit);
