import React, { useState, useEffect, Fragment } from 'react';
/**Importar componentes select */
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MethodGet from '../../Config/Services';

export default function SelectTypeClient(props) {
	const [error, guardarError] = useState(true);
	const [tipos, saveType] = useState([]);

const type_clientes = [
	{ type_client: '1', name: 'Cliente General' },
	{ type_client: '2', name: 'Cliente Alumno' },
];

	useEffect(() => {
		saveType(type_clientes);
		if (props.nameTypeClients) {
			guardarError(false);
		}
	}, [error]);
	
	const detectarCambiosTypeClient = (e) => {
		if (e.target.value) {
			guardarError(false);
		}
		props.detectarCambiosTypeClient(e);
	};

	return (
		<Fragment>
			<InputLabel fullWidth>Tipo de Cliente*</InputLabel>
			{props.nameTypeClients ? (
				<Select
					label="Tipo de Cliente"
					fullWidth
					onChange={detectarCambiosTypeClient}
					value={props.nameTypeClients.type_client}
				>
					<MenuItem disabled>Selecciona el tipo de cliente</MenuItem>
					{tipos.map((tipo) => (
						<MenuItem key={tipo.type_client} value={tipo.type_client}>
							{tipo.name}
						</MenuItem>
					))}
				</Select>
			) : (
				<Select
					label="Sucursales"
					fullWidth
					onChange={detectarCambiosTypeClient}
				>
					<MenuItem disabled>Selecciona el tipo de cliente</MenuItem>
					{tipos.map((tipo) => (
						<MenuItem key={tipo.type_client} value={tipo.type_client}>
							{tipo.name}
						</MenuItem>
					))}
				</Select>
			)}

			{error ? (
				<p style={{ color: 'red' }}>El tipo de cliente es un Campo Requerido</p>
			) : null}
		</Fragment>
	);
}





	
