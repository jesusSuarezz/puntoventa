import React, { useState, useEffect, Fragment } from 'react';
/**Importar configuracion axios */
import clienteAxios from '../../Config/Axios';
/**Importar componentes select */
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

export default function SelectProducts(props) {
	const [products, saveProducts] = useState([]);
	const [error, guardarError] = useState(true);
	useEffect(() => {
		const getProducts = async () => {
			await clienteAxios
				.get('/products')
				.then((res) => {
					saveProducts(res.data.data);
				})
				.catch((error) => {
					console.log(error);
				});

			if (props.productId) {
				guardarError(false);
			}
		};
		getProducts();
	}, []);

	const detectarCambiosProduct = (e) => {
		if (e.target.value) {
			guardarError(false);
		}
		props.detectarCambiosProduct(e);
	};

	return (
		<Fragment>
			<InputLabel fullWidth>Productos*</InputLabel>
			{props.productId ? (
				<Select
					label="Productos"
					fullWidth
					onChange={detectarCambiosProduct}
					value={props.productId.id_product}
				>
					<MenuItem disabled>Selecciona el producto</MenuItem>
					{products.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			) : (
				<Select label="Productos" fullWidth onChange={detectarCambiosProduct}>
					<MenuItem disabled>Selecciona el producto</MenuItem>
					{products.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			)}

			{error ? (
				<p style={{ color: 'red' }}>El producto es un Campo Requerido</p>
			) : null}
		</Fragment>
	);
}
