import React, { useState, useEffect, Fragment } from 'react';
/**Importar configuracion axios */
import clienteAxios from '../../Config/Axios';
/**Importar componentes select */
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

export default function SelectCourse(props) {
	const [courses, saveCourses] = useState([]);
	const [error, guardarError] = useState(true);
	useEffect(() => {
		const getOffices = async () => {
			await clienteAxios
				.get('/branches')
				.then((res) => {
					saveCourses(res.data.data);
				})
				.catch((error) => {
					console.log(error);
				});

			if (props.officeId) {
				guardarError(false);
			}
		};
		getOffices();
	}, []);

	const detectarCambioCourse = (e) => {
		if (e.target.value) {
			guardarError(false);
		}
		props.detectarCambioCourse(e);
	};

	return (
		<Fragment>
			<InputLabel fullWidth>Cursos*</InputLabel>
			{props.officeId ? (
				<Select
					label="Cursos"
					fullWidth
					onChange={detectarCambioCourse}
					value={props.officeId.id_branch_office}
				>
					<MenuItem disabled>Selecciona el curso</MenuItem>
					{courses.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			) : (
				<Select label="Cursos" fullWidth onChange={detectarCambioCourse}>
					<MenuItem disabled>Selecciona el curso</MenuItem>
					{courses.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			)}

			{error ? (
				<p style={{ color: 'red' }}>El curso un Campo Requerido</p>
			) : null}
		</Fragment>
	);
}
