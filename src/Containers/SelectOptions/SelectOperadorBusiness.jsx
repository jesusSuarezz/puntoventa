import React, { useState, useEffect, Fragment } from 'react';
/**Importar componentes select */
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MethodGet from '../../Config/Services';

export default function SelectOperadorBusiness(props) {
	const [error, guardarError] = useState(true);
	const [operators, saveOperators] = useState([]);

	const operadores = [
		{ operador: '1', name: '*' },
		{ operador: '2', name: '/' },
		{ operador: '3', name: '+' },
		{ operador: '4', name: '-' },
	];

	useEffect(() => {
		saveOperators(operadores);
		if (props.nameTypeClients) {
			guardarError(false);
		}
	}, [error]);

	const detectarCambioOperador = (e) => {
		if (e.target.value) {
			guardarError(false);
		}
		props.detectarCambioOperador(e);
	};

	return (
		<Fragment>
			<InputLabel fullWidth>Tipo de Operador*</InputLabel>
			{props.nameTypeClients ? (
				<Select
					label="Tipo de Operador"
					fullWidth
					onChange={detectarCambioOperador}
					value={props.nameTypeClients.operador}
				>
					<MenuItem disabled>Selecciona el tipo de Operador</MenuItem>
					{operators.map((operator) => (
						<MenuItem key={operator.operador} value={operator.operador}>
							{operator.name}
						</MenuItem>
					))}
				</Select>
			) : (
				<Select
					label="Sucursales"
					fullWidth
					onChange={detectarCambioOperador}
				>
					<MenuItem disabled>Selecciona el operator de Operador</MenuItem>
					{operators.map((operator) => (
						<MenuItem key={operator.operador} value={operator.operador}>
							{operator.name}
						</MenuItem>
					))}
				</Select>
			)}

			{error ? (
				<p style={{ color: 'red' }}>El tipo de Operador es un Campo Requerido</p>
			) : null}
		</Fragment>
	);
}
