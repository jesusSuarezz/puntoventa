import React, { useState, useEffect, Fragment } from 'react';
/**Importar configuracion axios */
import clienteAxios from '../../Config/Axios';
/**Importar componentes select */
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

export default function SelectClient(props) {
	const [clients, saveClients] = useState([]);
	const [error, guardarError] = useState(true);
	useEffect(() => {
		const getClients = async () => {
			await clienteAxios
				.get('/clients')
				.then((res) => {
					saveClients(res.data.data);
				})
				.catch((error) => {
					console.log(error);
				});

			if (props.client) {
				guardarError(false);
			}
		};
		getClients();
	}, []);

	const detectarCambioClient = (e) => {
		if (e.target.value) {
			guardarError(false);
		}
		props.detectarCambioClient(e);
	};

	return (
		<Fragment>
			<InputLabel fullWidth>Cliente*</InputLabel>
			{props.client ? (
				<Select
					label="Clientes"
					fullWidth
					onChange={detectarCambioClient}
					value={props.client.id_client}
				>
					<MenuItem disabled>Selecciona el cliente</MenuItem>
					{clients.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			) : (
				<Select label="Clientes" fullWidth onChange={detectarCambioClient}>
					<MenuItem disabled>Selecciona el cliente</MenuItem>
					{clients.map((office) => (
						<MenuItem key={office.id} value={office.id}>
							{office.name}
						</MenuItem>
					))}
				</Select>
			)}

			{error ? (
				<p style={{ color: 'red' }}>El Cliente es un Campo Requerido</p>
			) : null}
		</Fragment>
	);
}
