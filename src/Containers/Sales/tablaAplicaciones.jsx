import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import {Link} from 'react-router-dom';
///Importacion de TABS
import 'react-tabs/style/react-tabs.css';

/**Componentes Tabla */
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import { Button, Tooltip } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({

    root: {
        display: 'flex',
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    table: {
        minWidth: 650,
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingLeft: theme.spacing(18),
        paddingBottom: theme.spacing(4),
        paddingInlineEnd: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        marginBlockEnd: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    input: {
        display: 'none',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paperModal: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    formControl: {
		margin: theme.spacing(1),
		width:'100%'
	},
}));

function MisAplicaciones() {
    const classes = useStyles();

    return (
        <Fragment>
            <TableContainer >
                <Table>
                    <TableHead>
                    <TableRow >
                            <TableCell align="center" style={{color:"#0d47a1"}} >FOLIO VENTA</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >NOMBRE</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >MANICURISTA</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >FECHA</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >HORA INICIO</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >HORA FINAL</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >TOTAL</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >METODO DE PAGO</TableCell>
                            <TableCell align="center" style={{color:"#0d47a1"}} >OPCIONES</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >--</TableCell>
                            <TableCell align="center"  >
                        <Fragment>
                            <Link to={`Editar_VentaAplicacion/`}>
                                <Button
                                    style={{ textTransform: 'none' }}
                                    size='small'
                                >
                                    <Tooltip title="Editar Venta" aria-label="Editar Venta">
                                        <EditIcon style={{ color: 'blue' }} />
                                    </Tooltip>
                                </Button>
                            </Link>  
                            <Link to={`Detalle_AplicacionUñas/`}>
                                <Button
                                    style={{ textTransform: 'none' }}
                                    size='small'
                                >
                                    <Tooltip title="Detalle de Venta" aria-label="Detalle de Venta">
                                        <VisibilityIcon style={{ color: 'indigo' }} />
                                    </Tooltip>
                                </Button>
                            </Link>              
                                <Button
                                    style={{ textTransform: 'none' }}
                                    size='small'
                                >
                                    <Tooltip title="Eliminar Venta" aria-label="Eliminar Venta">
                                        <DeleteIcon style={{ color: 'red' }} />
                                    </Tooltip>
                                </Button>
                        </Fragment>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </Fragment>
    );
}

export default MisAplicaciones;