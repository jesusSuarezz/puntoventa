import React, { useState, useEffect } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import { useForm } from 'react-hook-form';
/**importar spinner */
import Spinner from '../../Complements/Spinner';
/**Importar MethodPut */
import MethodGet, { MethodPut } from '../../Config/Services';

/**Importar Sweetalert2 * */
import Swal from 'sweetalert2';
/**Habilitar redirecciones */
import { withRouter } from 'react-router-dom';
import SelectBranchOffice from '../SelectOptions/SelectBranchOffice';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
}));
const name = [{ title: 'Studio GlamOur' }];

function CourseEdit(props) {
	//Trabajar con el Spinner
	const [cargando, spinnerCargando] = useState(false);
	const { id } = props.match.params; //toma el id de la URL

	const classes = useStyles();

	// const [operator, setOperator] = useState({});
	const [course, saveCourse] = useState({});

	//Trabajar con el state de office
	const [officeId, saveOffice] = useState({
		id_branch_office: '',
	});

	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();

	//Extraer valores de course(destructuracion)
	const { name, capacity } = course;

	//Funciones para almacenar el id de classroom
	const detectarCambiosOffice = (e) => {
		saveOffice({ id_branch_office: e.target.value });
	};

	//obtener los campos de la sucursal
	useEffect(() => {
		let url = `/courses/${id}`;
		MethodGet(url)
			.then((res) => {
				saveCourse(res.data.data);
				saveOffice({ id_branch_office: res.data.data.id_branch_office });
				spinnerCargando(true);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	//Guardar los camios de la sucursal
	const onSubmit = (data, e) => {
		e.preventDefault();

		const dat = {
			...data,
			...officeId,
		};

		let url = `/courses/${id}`;
		MethodPut(url, dat)
			.then((res) => {
				Swal.fire({
					title: 'Curso Editado Exitosamente',
					text: res.data.message,
					icon: 'success',
					timer: 2000,
					showConfirmButton: false,
				});
				props.history.push('/Cursos');
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error',
					icon: error.response.data.error,
					title: error,
				});
			});
	};

	//spinner de carga
	if (!cargando) return <Spinner />;

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Curso
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="name"
								label="Nombre Curso"
								name="name"
								autoComplete="name"
								autoFocus
								error={!!errors.name}
								defaultValue={name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre del curso es requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: 'Unicamente carácteres alfabéticos',
									},
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="capacity"
								label="Capacidad"
								type="number"
								name="capacity"
								autoComplete="name"
								defaultValue={capacity}
								error={!!errors.capacity}
								inputRef={register({
									required: {
										value: true,
										message: 'La capacidad es requerida',
									},
									maxLength: {
										value: 5,
										message: 'Maximo 5 caracteres',
									},
								})}
							/>
							<p>{errors?.capacity?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6} style={{ paddingTop: 24 }}>
							<FormControl
								variant="outlined"
								className={classes.FormControl}
								fullWidth
							>
								<SelectBranchOffice
									officeId={officeId}
									detectarCambiosOffice={detectarCambiosOffice}
								/>
							</FormControl>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}

export default withRouter(CourseEdit);
