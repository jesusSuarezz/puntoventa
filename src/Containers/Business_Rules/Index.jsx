import React, {useEffect, useState} from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Box from '@material-ui/core/Box';

/**Importar componentes tablas */
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import MaterialTable from 'material-table';
/**Importar metodo Get */
import MethodGet,{MethodDelete} from '../../Config/Services';
/**Importar Sweetalert2 * */
import Swal from 'sweetalert2';
/**importar spinner */
import Spinner from '../../Complements/Spinner';

const useStyles = makeStyles((theme) => ({

    root: {
        display: 'flex',
    }, 
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingLeft: theme.spacing(18),
        paddingBottom: theme.spacing(4),
        paddingInlineEnd: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        marginBlockEnd: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    input: {
        display: 'none',
    },
    typography:{
      margin: theme.spacing(0,1,2,2),
    }
}));

const BusinessRulesIndex = props => {
    const classes = useStyles();
	
	//proveedores = state, guardarRules =funcion para guardar en el state;
   const [ business, guardarRules ] = useState([]);
   const [ cargando, spinnerCargando ] = useState(false);
   
     //query a la api
	useEffect(() => {
			let url = '/businessRules'
			MethodGet(url).then(res=>{
				guardarRules(res.data.data);
				spinnerCargando(true);
			}).catch(error=>{
				console.log(error);
				spinnerCargando(true);
			})
	}, []);
    
     const deleteBussinessRule = (id) => {
				Swal.fire({
					title: '¿Estás seguro?',
					text: 'Una regla de negocio eliminada no se puede recuperar',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Si, Eliminar',
					cancelButtonText: 'No, Cancelar',
				}).then((result) => {
					if (result.value) {
						// eliminar en la rest api
						let url = `/businessRules/${id}`;
						MethodDelete(url).then((res) => {
							if (res.status === 200) {
								Swal.fire('Eliminado', res.data.mensaje, 'success');
							}
						});
					}
				});
			};

    //  spinner de carga 
   if(!cargando) return <Spinner/>
 
    return (
			<LayoutDashboard>
				<div className={classes.root}>
					<CssBaseline />
					<main className={classes.content}>
						<div className={classes.appBarSpacer} />
						<Container className={classes.container}>
							<Grid Container spacing={3}>
								<Typography
									component="h1"
									variant="h4"
									align="center"
									className={classes.typography}
								>
									<Box align="right">
										<Fab
											color="primary"
											aria-label="add"
											size="small"
											href="/Agregar_Regla"
										>
											<AddIcon />
										</Fab>
									</Box>
								</Typography>
								<MaterialTable
									title="Mis Reglas de Negocio"
									columns={[
										{ title: 'CLAVE', field: 'id' },
										{ title: 'TIPO DE CLIENTE', field: 'type_client' },
										{ title: 'OPERADOR', field: 'operator' },
										{ title: 'DESCUENTO', field: 'discount' },
									]}
									data={business}
									options={{
										headerStyle: {
											backgroundColor: 'orange',
											color: '#FFF',
										},

										actionsColumnIndex: -1,
									}}
									actions={[
										{
											icon: 'edit',
											tooltip: 'Editar Usuario',
										},
										(rowData) => ({
											icon: 'delete',
											tooltip: 'Eliminar Usuario',
											onClick: () => deleteBussinessRule(rowData.id),
										}),
									]}
									localization={{
										pagination: {
											labelDisplayedRows: '{from} - {to} de {count}',
											labelRowsSelect: 'Columnas',
											firstTooltip: 'Primera Pagina',
											previousTooltip: 'Anterior',
											nextTooltip: 'Siguiente',
											lastTooltip: 'Ultima Página',
										},
										toolbar: {
											searchTooltip: 'Buscar',
											searchPlaceholder: 'Buscar',
										},
										header: {
											actions: 'OPCIONES',
										},
										body: {
											emptyDataSourceMessage: 'No Hay Descuentos que Mostrar',
											filterRow: {
												filterTooltip: 'Buscar',
											},
										},
									}}
								/>
							</Grid>
						</Container>
					</main>
				</div>
			</LayoutDashboard>
		);
}

export default BusinessRulesIndex;