import React from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Autocomplete from '@material-ui/lab/Autocomplete';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
}));

const type_client = [{ title: 'Cliente General' }, { title: 'Cliente Alumno' }];

const Operator = [
	{ title: '*' },
	{ title: '/' },
	{ title: '+' },
	{ title: '-' },
];

const BusinessRulesEdit = (props) => {
	const classes = useStyles();
	const [operator, setOperator] = React.useState('');

	const handleChange = (event) => {
		setOperator(event.target.value);
	};

	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();
	const onSubmit = (data, e) => {
		e.preventDefault();
		console.log(data);
		props.history.push('/');
	};

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Regla de Negocio
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6}>
							<Autocomplete
								id="combo-box-demo"
								fullWidth
								options={type_client}
								getOptionLabel={(option) => option.title}
								renderInput={(params) => (
									<TextField
										{...params}
										label="Tipo de Cliente"
										variant="outlined"
									/>
								)}
							/>
						</Grid>

						<Grid item xs={12} sm={6}>
							<Autocomplete
								id="combo-box-demo"
								fullWidth
								options={Operator}
								getOptionLabel={(option) => option.title}
								renderInput={(params) => (
									<TextField {...params} label="Operador" variant="outlined" />
								)}
							/>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="discount"
								label="Porcentaje de Descuento"
								name="discount"
								autoComplete="name"
								autoFocus
								type="number"
								error={!!errors.discount}
								inputRef={register({
									required: {
										value: true,
										message: 'El porcentaje de descuento es requerido',
									},
								})}
							/>
							<p>
								{errors.discount && 'El porcentaje de descuento es Requerido'}
							</p>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
};

export default BusinessRulesEdit;
