import React from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useForm } from "react-hook-form";


import { MethodPost } from '../../Config/Services';
/**Importar Sweetalert2 * */
import Swal from 'sweetalert2';

const useStyles = makeStyles((theme) => ({

  root: {
    flexGrow: 1,
  },
  container: {
    paddingBottom: theme.spacing(4),
    paddingInlineEnd: theme.spacing(2),
    paddingTop: theme.spacing(4),
    paddingLeft: theme.spacing(30),
  },
  typography: {
    paddingTop: theme.spacing(4),
    margin: theme.spacing(1),
    paddingLeft: theme.spacing(30)

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(9),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginBlockEnd: theme.spacing(1),
  },
  submit: {
   
    background: "#3537DB",
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      background: "#5902CF"
  },
  color: 'white'
  },
  ContainerSubmit: {
    marginLeft: theme.spacing(30),
    marginTop: theme.spacing(1)
  },
  formControl: {
		margin: theme.spacing(1),
		width:'100%'
	},
}));
const name = [
  { title: 'Uñas Acrilicas'}
]

export default function ScheduleAdd(props) {
  const classes = useStyles();
  
   const [operator, setOperator] = React.useState({});

		const detectarOperador = (e) => {
			setOperator({ operator: e.target.value });
		};

		//Funcion para validaciones
		const { register, handleSubmit, errors } = useForm();
		const onSubmit = (data, e) => {
			e.preventDefault();

			//Guardar en la base de datos
			const dat = {
				...operator,
				...data,
			};

			let url = '/schedules';
			//Definir sweetalert
			const Toast = Swal.mixin({
				toast: true,
				position: 'bottom',
				showConfirmButton: false,
				timer: 6000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener('mouseenter', Swal.stopTimer);
					toast.addEventListener('mouseleave', Swal.resumeTimer);
				},
			});
			MethodPost(url, dat)
				.then((res) => {
					Toast.fire({
						icon: 'success',
						title: 'Programación de Nuevo Curso Registrado Exitosamente',
					});
					props.history.push('/Calendarios');
				})
				.catch((error) => {
					Toast.fire({
						icon: 'error',
						title: error.response.data.error,
					});
				});
		};

  return (
    <LayoutDashboard>
      <div className={classes.root}>

        <form  onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
          <Box>
            <Typography component="h1" variant="h5" align="center" className={classes.typography}>
              Agregar Programacion de Curso
            </Typography>
          </Box>
          <Grid container spacing={2} className={classes.container}>
                <Grid item xs={12} sm={6} style={{ paddingTop: 24 }}>
                        <Autocomplete
                            id="combo-box-demo"
                            fullWidth
                            options={name}
                            getOptionLabel={(option) => option.title}
                            renderInput={(params) => <TextField {...params} label="Programa" variant="outlined" />}
                        />
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="date"
                    label="Fecha"
                    type="date"
                    defaultValue="2017-05-24"
                    name="date"
                    autoComplete="name"
                    autoFocus
                    error={!!errors.date}
                    inputRef={register({
                      required:{
                        value:true,
                        message: 'La Fecha es requerida',
                      },
                    })}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <p>{errors?.date?.message}</p>
                </Grid>

                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="start_time"
                    label="Hora Inicial"
                    name="start_time"
                    autoComplete="name"
                    type="time"
                    defaultValue="07:30"
                    autoFocus
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      step: 300, // 5 min
                    }}
                    error={!!errors.start_time}
                    inputRef={register({
                      required:{
                        value:true,
                        message: 'La Hora de Inicio es requerido',
                      },
                    })}
                  />
                  <p>{errors?.start_time?.message}</p>
                </Grid>
                
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="end_time"
                    label="Hora que Terminó"
                    name="end_time"
                    autoComplete="name"
                    type="time"
                    defaultValue="08:30"
                    autoFocus
                    InputLabelProps={{
                      shrink: true,
                    }}
                    inputProps={{
                      step: 300, // 5 min
                    }}
                    error={!!errors.end_time}
                    inputRef={register({
                      required:{
                        value:true,
                        message: 'La Hora que Terminó es requerido',
                      },
                    })}
                  />
                  <p>{errors?.end_time?.message}</p>
                </Grid>

                </Grid>
					
					<Grid container spacing={2} >
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								<label>GUARDAR</label>
							</Button>
						</Grid>
					</Grid>
        </form>
      </div>
    </LayoutDashboard>
  );
}