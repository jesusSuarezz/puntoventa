import React, { useEffect, useState, useContext, Fragment } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Container } from '@material-ui/core';
import { makeStyles, withStyles} from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useForm } from "react-hook-form";


/**importar spinner */
import Spinner from "../../Complements/Spinner";
/**Importar Sweetalert2 * */
import Swal from "sweetalert2";
/**Habilitar redirecciones */
import { withRouter } from "react-router-dom";
import MethodGet, { MethodPut } from '../../Config/Services';
import FormControl from '@material-ui/core/FormControl';

import SelectMethodsPayments from '../SelectOptions/SelectMethodsPayments'

const useStyles = makeStyles((theme) => ({

    root: {
        flexGrow: 1,
    },
    container: {
        paddingBottom: theme.spacing(4),
        paddingInlineEnd: theme.spacing(2),
        paddingTop: theme.spacing(4),
        paddingLeft: theme.spacing(30),
    },
    typography: {
        paddingTop: theme.spacing(4),
        margin: theme.spacing(1),
        paddingLeft: theme.spacing(30)

    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(9),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        marginBlockEnd: theme.spacing(1),
    },
    submit: {
        background: "#3537DB",
        "&:hover": {
          //you want this to be the same as the backgroundColor above
          background: "#5902CF"
      },
      color: 'white'
	  },
	  ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1)
	},
	formControl: {
		margin: theme.spacing(1),
		width:'100%'
	},
}));
const name = [
    { title: 'BBVA Bancomer' },
    { title: 'Santander' },
    { title: 'Scotiabank' },
    { title: 'Banco Azteca' },
    { title: 'Banjercito' },
    { title: 'CitiBanamex' },
    { title: 'Afirme' },
    { title: 'Banorte' },
    { title: 'Farmacias del Ahorro' },
    { title: 'Farmacias Guadalajara' },
    { title: 'HSBC' },
    { title: 'Telecomm' },
    { title: 'Oxxo' },
    { title: 'Efectivo' },
  ]
  const name_client = [
    { title: 'Jimena Tovar Hernandez'}
  ]

 function PaymentEdit(props) {
   const classes = useStyles();
		{
			/**Este Es para abrir el Modal de Metodos de pago */
		}

		const [open, setOpen] = React.useState(false);

		const handleOpen = () => {
			setOpen(true);
		};
		const handleClose = () => {
			setOpen(false);
		};
		{
			/**Este es para abrir el modal de agregar cliente */
		}
		const [openaddClient, setOpenaddClient] = useState(false);

		const handleOpenaddClient = () => {
			setOpenaddClient(true);
		};

		const handleCloseaddClient = () => {
			setOpenaddClient(false);
		};

		// --------------------------- Editar Cita
		//obtener el id de service_schedule
		const { id } = props.match.params;

		//Trabajar con el state para guardar la informacion de payment
		const [payment, savePayment] = useState({});

		const { register, handleSubmit, errors } = useForm();

		//Trabajar con el spinner
		const [cargando, spinnerCargando] = useState(false);

		//Trabajar con el state de office
		const [method, saveMethod] = useState({
			id_payment_method: '',
		});

		//extraer las propiedades del objeto Desctructuracion
		const { quantity } = payment;

		//Funciones para almacenar el id de payment
		const detectarCambiosMethods = (e) => {
			saveMethod({ id_payment_method: e.target.value });
		};

		//Query para consultar el cliente
		useEffect(() => {
			let url = `/payments/${id}`;
			MethodGet(url)
				.then((res) => {
					savePayment(res.data.data);
					saveMethod({ id_payment_method: res.data.data.id_payment_method });
					spinnerCargando(true);
				})
				.catch((error) => {
					console.log(error);
				});
		}, []);

		//Guardar los cambios del cliente
		const onSubmit = (data, e) => {
			e.preventDefault();
			const dat = {
				...data,
				...method,
			};
			let url = `/payments/${id}`;
			MethodPut(url, dat)
				.then((res) => {
					Swal.fire({
						title: 'Cita Editada Exitosamente',
						text: res.data.message,
						icon: 'success',
						timer: 2000,
						showConfirmButton: false,
					});
					props.history.push('/Citas');
				})
				.catch((error) => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.error,
						icon: 'error',
					});
				});
		};

		//  spinner de carga
		if (!cargando) return <Spinner />;

    return (
			<LayoutDashboard>
				<div className={classes.root}>
					<form
						onSubmit={handleSubmit(onSubmit)}
						className={classes.form}
						noValidate
					>
						<Box>
							<Typography
								component="h1"
								variant="h5"
								align="center"
								className={classes.typography}
							>
								Editar Pago
							</Typography>
						</Box>
						<Grid container spacing={2} className={classes.container}>
							<Grid item xs={12} sm={6}>
								<Autocomplete
									id="combo-box-demo"
									fullWidth
									options={name_client}
									getOptionLabel={(option) => option.title}
									renderInput={(params) => (
										<TextField {...params} label="Cliente" variant="outlined" />
									)}
								/>
							</Grid>

							<Grid item xs={12} sm={6}>
								<FormControl variant="outlined" fullWidth>
									<SelectMethodsPayments
										method={method}
										detectarCambiosMethods={detectarCambiosMethods}
									/>
								</FormControl>
							</Grid>

							<Grid item xs={12} sm={6}>
								<TextField
									variant="outlined"
									margin="normal"
									required
									fullWidth
									id="quantity"
									label="Cantidad"
									name="quantity"
									type="number"
									defaultValue={quantity}
									autoComplete="name"
									error={!!errors.quantity}
									inputRef={register({
										required: {
											value: true,
											message: 'La cantidad es requerida',
										},
										maxLength: {
											value: 5,
											message: 'Maximo 5 digitos',
										},
									})}
								/>
								<p>{errors?.quantity?.message}</p>
							</Grid>
						</Grid>

						<Grid container spacing={2}>
							<Grid item xs={3} className={classes.ContainerSubmit}>
								<Button
									type="submit"
									fullWidth
									variant="outlined"
									className={classes.submit}
								>
									ACTUALIZAR
								</Button>
							</Grid>
						</Grid>
					</form>
				</div>
			</LayoutDashboard>
		);
}

export default withRouter(PaymentEdit)