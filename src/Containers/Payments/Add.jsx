import React, { useState } from 'react';

import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Container } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useForm } from 'react-hook-form';
import FormControl from '@material-ui/core/FormControl';

import Swal from 'sweetalert2';

import SelectMethodsPayments from '../SelectOptions/SelectMethodsPayments';
import SelectClient from '../SelectOptions/SelectClient';
import { withRouter } from 'react-router-dom';
import { MethodPost } from '../../Config/Services';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
}));
const name = [
	{ title: 'BBVA Bancomer' },
	{ title: 'Santander' },
	{ title: 'Scotiabank' },
	{ title: 'Banco Azteca' },
	{ title: 'Banjercito' },
	{ title: 'CitiBanamex' },
	{ title: 'Afirme' },
	{ title: 'Banorte' },
	{ title: 'Farmacias del Ahorro' },
	{ title: 'Farmacias Guadalajara' },
	{ title: 'HSBC' },
	{ title: 'Telecomm' },
	{ title: 'Oxxo' },
	{ title: 'Efectivo' },
];

const name_client = [{ title: 'Jimena Tovar Hernandez' }];

function PaymentAdd(props) {
	const classes = useStyles();


	const [method, saveMethods] = useState({
		id_payment_method: '',
	});

	const [client, saveClient] = useState({
		id_client: '',
	});

	
	const detectarCambiosMethods = (e) => {
		saveMethods({ id_payment_method: e.target.value });
	};
	
	const detectarCambioClient = (e) => {
		saveClient({ id_client: e.target.value });
	};

	const { register, handleSubmit, errors } = useForm();

	const onSubmit = (data, e) => {
		e.preventDefault();

		if (method.id_payment !== '' && client.id_client !== '') {
			const dat = {
				...data,
				...method,
				...client
			};

			let url = '/payments';
			MethodPost(url, dat)
				.then((res) => {
					Swal.fire({
						title: 'Método de Pago Registrado Exitosamente',
						text: res.data.message,
						icon: 'success',
						timer: 2000,
						showConfirmButton: false,
					});

					props.history.push('/Pagos');
				})
				.catch((error) => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.error,
						icon: 'error',
					});
				});
		} else {
			Swal.fire({
				title: 'Error',
				text: 'Todos los campos son Obligatorios',
				icon: 'error',
			});
		}
	};

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Agregar Pago
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectClient detectarCambioClient={detectarCambioClient} />
							</FormControl>
						</Grid>

						<Grid item xs={12} sm={6}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectMethodsPayments
									detectarCambiosMethods={detectarCambiosMethods}
								/>
							</FormControl>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="quantity"
								label="Cantidad"
								name="quantity"
								autoComplete="quantity"
								type="number"
								error={!!errors.quantity}
								inputRef={register({
									required: {
										value: true,
										message: 'La cantidad es requerida',
									},
									maxLength: {
										value: 5,
										message: 'Maximo 5 digitos',
									},
								})}
							/>
							<p>{errors?.quantity?.message}</p>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								GUARDAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}

export default withRouter(PaymentAdd);
