import React, { useState, useEffect } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form';
import { Grid, Box, Paper } from '@material-ui/core';

/**importar spinner */
import Spinner from '../../Complements/Spinner';
/**Importar MethodPut */
import MethodGet, { MethodPut } from '../../Config/Services';

/**Importar Sweetalert2 * */
import Swal from 'sweetalert2';
/**Habilitar redirecciones */
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

  root: {
    flexGrow: 1,
  },
  container: {
    paddingBottom: theme.spacing(4),
    paddingInlineEnd: theme.spacing(2),
    paddingTop: theme.spacing(4),
    paddingLeft: theme.spacing(30),
  },
  typography: {
    paddingTop: theme.spacing(4),
    margin: theme.spacing(1),
    paddingLeft: theme.spacing(30)

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(9),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginBlockEnd: theme.spacing(1),
  },
  submit: {
    background: "#3537DB",
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      background: "#5902CF"
  },
  color: 'white'
  },
  formControl: {
	margin: theme.spacing(1),
	width:'100%'
},
selectEmpty: {
    marginTop: theme.spacing(2),
},
ContainerSubmit: {
	marginLeft: theme.spacing(30),
	marginTop: theme.spacing(1)
}
}));



function GraduateEdit(props) {
  const classes = useStyles();
	const [cargando, spinnerCargando] = useState(false);

  
  const { id } = props.match.params; //toma el id de la URL


	// const [operator, setOperator] = useState({});
	const [graduates, saveGraduate] = useState({});

	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();

	//Extraer valores de graduates(destructuracion)
	const { name, description } = graduates;

	//Guardar los camios de la sucursal
	const onSubmit = (data, e) => {
		e.preventDefault();

		const dat = {
			...data,
		};

		let url = `/graduates/${id}`;
		MethodPut(url, dat)
			.then((res) => {
				Swal.fire({
					title: 'Diplomado Editado Exitosamente',
					text: res.data.message,
					icon: 'success',
					timer: 2000,
					showConfirmButton: false,
				});
				props.history.push('/Diplomados');
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error',
					icon: error.response.data.error,
					title: error,
				});
			});
	};

	//obtener los campos de la sucursal
	useEffect(() => {
		let url = `/graduates/${id}`;
		MethodGet(url)
			.then((res) => {
				saveGraduate(res.data.data);
				spinnerCargando(true);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	//spinner de carga
	if (!cargando) return <Spinner />;

  return (
		<LayoutDashboard>
			<div onSubmit={handleSubmit(onSubmit)} className={classes.root}>
				<form className={classes.form} noValidate>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Diplomados
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						{/* <Grid item xs={12} sm={6} style={{paddingTop:24}}>
                        <Autocomplete
                            id="combo-box-demo"
                            fullWidth
                            options={name_classroom}
                            getOptionLabel={(option) => option.title}
                            renderInput={(params) => <TextField {...params} label="Salon" variant="outlined" />}
                        />
                    </Grid> */}
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="name"
								label="Nombre"
								name="name"
								autoComplete="name"
								autoFocus
								defaultValue={name}
								error={!!errors.name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre  requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: "Unicamente carácteres alfabéticos"
									  }
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="description"
								label="Descripcion"
								name="description"
								autoComplete="name"
								autoFocus
								defaultValue={description}
								error={!!errors.description}
								inputRef={register({
									required: {
										value: true,
										message: 'La descripcion es requerido',
									},
									minLength: {
										value: 1,
										message: 'Minimo 5 caracteres',
									},
									maxLength: {
										value: 45,
										message: 'Maximo 45 caracteres',
									},
								})}
							/>
							<p>{errors?.description?.message}</p>
						</Grid>

						</Grid>
					
					<Grid container spacing={2} >
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}
export default withRouter(GraduateEdit)