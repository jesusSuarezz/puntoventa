import React, { useState } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import { useForm } from 'react-hook-form';
/**Habilitar redirecciones */
import { withRouter } from 'react-router-dom';

import Swal from 'sweetalert2';

import { MethodPost } from '../../Config/Services';

import SelectBranchOffice from '../SelectOptions/SelectBranchOffice';
import SelectTypeClient from '../SelectOptions/SelectTypeClient';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	formControl: {
		margin: theme.spacing(1),
		width:'100%'
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1)
	}
}));

function ClientAdd(props) {
	const classes = useStyles();

	const [nameOffice, guardarSucursal] = useState({
		id_branch_office: '',
	});

	const [nameTypeClients, saveType] = useState({
		type_client: '',
	});

	const detectarCambiosOffice = (e) => {
		guardarSucursal({ id_branch_office: e.target.value });
	};

	const detectarCambiosTypeClient = (e) => {
		saveType({ type_client: e.target.value });
	};
	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();

	const onSubmit = (data, e) => {
		e.preventDefault();

		if (nameOffice.id_branch_office !== '' && nameTypeClients.type_client !== '') {
			const dat = {
				...data,
				...nameOffice,
				...nameTypeClients,
			};
			
			let url = '/clients';
			MethodPost(url, dat)
				.then((res) => {
					Swal.fire({
						title: 'Cliente Registrado Exitosamente',
						text: res.data.message,
						icon: 'success',
						timer: 2000,
						showConfirmButton: false,
					});

					props.history.push('/Clientes');
				})
				.catch((error) => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.error,
						icon: 'error',
					});
				});
		} else {
			Swal.fire({
				title: 'Error',
				text: 'Todos los campos son Obligatorios',
				icon: 'error',
			});
		}
	};

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Agregar Nuevo Cliente
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6} style={{ paddingTop: 16 }}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectBranchOffice
									detectarCambiosOffice={detectarCambiosOffice}
								/>
							</FormControl>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="name"
								label="Nombre Completo"
								name="name"
								autoComplete="name"
								autoFocus
								error={!!errors.name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre es requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 255,
										message: 'Maximo 255 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: "Unicamente carácteres alfabéticos"
									  }
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="number_phone"
								label="Telefono"
								name="number_phone"
								autoComplete="phone"
								type="number"
								autoFocus
								error={!!errors.number_phone}
								inputRef={register({
									required: {
										value: true,
										message: 'El telefono es requerido',
									},
									minLength: {
										value: 10,
										message: 'Mínimo 10 digitos',
									},
									maxLength: {
										value: 10,
										message: 'Maximo 10 digitos',
									},
								})}
							/>
							<p>{errors?.number_phone?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6} style={{ paddingTop: 16 }}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectTypeClient
									detectarCambiosTypeClient={detectarCambiosTypeClient}
								/>
							</FormControl>
						</Grid>

						</Grid>
					
					<Grid container spacing={2} >
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								GUARDAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}

export default withRouter(ClientAdd);
