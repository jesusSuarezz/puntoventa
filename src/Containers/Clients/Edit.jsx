import React, { useState, useEffect } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useForm } from 'react-hook-form';
import MethodGet, { MethodPut } from '../../Config/Services';
import Swal from 'sweetalert2';
import Spinner from '../../Complements/Spinner';
import { withRouter } from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';


import SelectBranchOffice from '../SelectOptions/SelectBranchOffice';
import SelectTypeClient from '../SelectOptions/SelectTypeClient';
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	container: {
		paddingBottom: theme.spacing(4),
		paddingInlineEnd: theme.spacing(2),
		paddingTop: theme.spacing(4),
		paddingLeft: theme.spacing(30),
	},
	typography: {
		paddingTop: theme.spacing(4),
		margin: theme.spacing(1),
		paddingLeft: theme.spacing(30),
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(9),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
		marginBlockEnd: theme.spacing(1),
	},
	submit: {
		background: '#3537DB',
		'&:hover': {
			//you want this to be the same as the backgroundColor above
			background: '#5902CF',
		},
		color: 'white',
	},
	formControl: {
		margin: theme.spacing(1),
		width: '100%',
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
	ContainerSubmit: {
		marginLeft: theme.spacing(30),
		marginTop: theme.spacing(1),
	},
}));

function ClientEdit(props) {
	const classes = useStyles();
	const [cargando, spinnerCargando] = useState(false);

	const { id } = props.match.params;

	//Funcion para validaciones
	const { register, handleSubmit, errors } = useForm();

	// const [operator, setOperator] = useState({});
	const [clients, guardarClient] = useState({});

	//Trabajar con el state de office
	const [officeId, saveOffice] = useState({
		id_branch_office: '',
	});

	const [nameTypeClients, guardarType] = useState({
		type_client: '',
	});

	const { name, number_phone } = clients;

	//Funciones para almacenar el id de Office
	const detectarCambiosOffice = (e) => {
		saveOffice({ id_branch_office: e.target.value });
	};

	const detectarCambiosTypeClient = (e) => {
		guardarType({ type_client: e.target.value });
	};

	const handleChange = (e) => {
		guardarClient({ operator: e.target.value });
	};

	useEffect(() => {
		let url = `/clients/${id}`;

		MethodGet(url)
			.then((res) => {
				guardarClient(res.data.data);
				saveOffice({ id_branch_office: res.data.data.id_branch_office });
				guardarType({ type_client: res.data.data.type_client });
				spinnerCargando(true);
			})
			.catch((error) => {
				console.log(error);
			});
	}, []);

	const onSubmit = (data, e) => {
		e.preventDefault();

		const dat = {
			...data,
			...officeId,
			...nameTypeClients,
		};

		let url = `/clients/${id}`; //cambiar la url completa
		MethodPut(url, dat)
			.then((res) => {
				Swal.fire({
					title: 'Cliente Editado Exitosamente',
					text: res.data.message,
					icon: 'success',
					timer: 2000,
					showConfirmButton: false,
				});
				props.history.push('/Clientes');
			})
			.catch((error) => {
				Swal.fire({
					title: 'Error',
					text: error.response.data.error,
					icon: 'error',
				});
			});
	};

	//  spinner de carga
	if (!cargando) return <Spinner />;

	return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Cliente
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6} style={{ paddingTop: 16 }}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectBranchOffice
									officeId={officeId}
									detectarCambiosOffice={detectarCambiosOffice}
								/>
							</FormControl>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="name"
								label="Nombre Completo"
								name="name"
								autoComplete="name"
								autoFocus
								defaultValue={name}
								error={!!errors.name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre es requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 255,
										message: 'Maximo 255 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: 'Unicamente carácteres alfabéticos',
									},
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="number_phone"
								label="Telefono"
								name="number_phone"
								autoComplete="phone"
								type="number"
								autoFocus
								defaultValue={number_phone}
								error={!!errors.number_phone}
								inputRef={register({
									required: {
										value: true,
										message: 'El telefono es requerido',
									},
									maxLength: {
										value: 10,
										message: 'Maximo 10 digitos',
									},
								})}
							/>
							<p>{errors?.number_phone?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6} style={{ paddingTop: 16 }}>
							<FormControl variant="outlined" className={classes.formControl}>
								<SelectTypeClient
									nameTypeClients={nameTypeClients}
									detectarCambiosTypeClient={detectarCambiosTypeClient}
								/>
							</FormControl>
						</Grid>
					</Grid>

					<Grid container spacing={2}>
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}
export default withRouter(ClientEdit);
