import React from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Grid, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Container } from '@material-ui/core';
import { makeStyles, withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import NativeSelect from '@material-ui/core/NativeSelect';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';

import { useForm } from "react-hook-form";

const useStyles = makeStyles((theme) => ({

  root: {
    flexGrow: 1,
  },
  container: {
    paddingBottom: theme.spacing(4),
    paddingInlineEnd: theme.spacing(2),
    paddingTop: theme.spacing(4),
    paddingLeft: theme.spacing(30),
  },
  typography: {
    paddingTop: theme.spacing(4),
    margin: theme.spacing(1),
    paddingLeft: theme.spacing(30)

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(9),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginBlockEnd: theme.spacing(1),
  },
  submit: {
    
    background: "#3537DB",
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      background: "#5902CF"
  },
  color: 'white'
  },
  ContainerSubmit: {
	marginLeft: theme.spacing(30),
	marginTop: theme.spacing(1)
},
formControl: {
	margin: theme.spacing(1),
	width:'100%'
},
selectEmpty: {
    marginTop: theme.spacing(2),
},
}));
const type = [
  { title: 'Aplicador'},
  { title: 'Vendedor'},
  { title: 'Limpieza'},
  { title: 'Maestro'},
]

const name = [
  { title: 'Studio GlamOur'},
]

export default function UserEdit(props) {
  const classes = useStyles();
  
  const { register, handleSubmit, errors } = useForm();
  const onSubmit = (data, e) => {
    e.preventDefault();
    console.log(data);
    props.history.push("/");
  };


  return (
		<LayoutDashboard>
			<div className={classes.root}>
				<form
					onSubmit={handleSubmit(onSubmit)}
					className={classes.form}
					noValidate
				>
					<Box>
						<Typography
							component="h1"
							variant="h5"
							align="center"
							className={classes.typography}
						>
							Editar Usuario
						</Typography>
					</Box>
					<Grid container spacing={2} className={classes.container}>
						<Grid item xs={12} sm={6} style={{ paddingTop: 24 }}>
							<Autocomplete
								id="combo-box-demo"
								fullWidth
								options={name}
								getOptionLabel={(option) => option.title}
								renderInput={(params) => (
									<TextField {...params} label="Sucursal" variant="outlined" />
								)}
							/>
						</Grid>

						<Grid item xs={12} sm={6}>
							<Autocomplete
								id="combo-box-demo"
								fullWidth
								options={type}
								getOptionLabel={(option) => option.title}
								renderInput={(params) => (
									<TextField
										{...params}
										label="Tipo de Usuario"
										variant="outlined"
									/>
								)}
							/>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="name"
								label="Nombre Completo"
								name="name"
								autoComplete="name"
								autoFocus
								error={!!errors.name}
								inputRef={register({
									required: {
										value: true,
										message: 'El nombre es requerido',
									},
									minLength: {
										value: 4,
										message: 'Minimo 4 caracteres',
									},
									maxLength: {
										value: 255,
										message: 'Maximo 255 caracteres',
									},
									pattern: {
										value: /^[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]+[A-Záéíóúñ ]$/i,
										message: "Unicamente carácteres alfabéticos"
									  }
								})}
							/>
							<p>{errors?.name?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="number_phone"
								label="Telefono"
								name="number_phone"
								autoComplete="phone"
								type="number"
								autoFocus
								error={!!errors.number_phone}
								inputRef={register({
									required: {
										value: true,
										message: 'El telefono es requerido',
									},
									maxLength: {
										value: 10,
										message: 'Maximo 10 digitos',
									},
								})}
							/>
							<p>{errors?.number_phone?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="email"
								label="Correo Electronico"
								name="email"
								autoComplete="name"
								autoFocus
								error={!!errors.email}
								inputRef={register({
									required: {
										value: true,
										message: 'El correo es requerido',
									},
									type: 'email',
									pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
								})}
							/>
							<p>{errors?.email?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="password"
								label="Contraseña"
								name="password"
								type="password"
								autoComplete="name"
								autoFocus
								error={!!errors.password}
								inputRef={register({
									required: {
										value: true,
										message: 'La contraseña es requerida',
									},
									minLength: {
										value: 8,
										message: 'Minimo 8 caracteres',
									},
									maxLength: {
										value: 16,
										message: 'Maximo 16 caracteres',
									},
								})}
							/>
							<p>{errors?.password?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								id="c_password"
								label="Confirmar Contraseña"
								name="c_password"
								type="password"
								autoComplete="name"
								autoFocus
								error={!!errors.c_password}
								inputRef={register({
									required: {
										value: true,
										message: 'Confirmar Contraseña es requerida',
									},
									minLength: {
										value: 8,
										message: 'Minimo 8 caracteres',
									},
									maxLength: {
										value: 16,
										message: 'Maximo 16 caracteres',
									},
								})}
							/>
							<p>{errors?.c_password?.message}</p>
						</Grid>

						<Grid item xs={12} sm={6}>
							<label htmlFor="outlined-button-file">
								<label variant="outlined" color="primary" component="span">
									Seleccionar imagen
								</label>
							</label>
							<input
								accept="image/*"
								className={classes.input}
								id="icon-button-file"
								type="file"
							/>
							<label htmlFor="icon-button-file">
								<IconButton
									color="primary"
									aria-label="upload-picture"
									component="span"
								>
									<PhotoCamera />
								</IconButton>
							</label>
						</Grid>

						

						</Grid>
					
					<Grid container spacing={2} >
						<Grid item xs={3} className={classes.ContainerSubmit}>
							<Button
								type="submit"
								fullWidth
								variant="outlined"
								className={classes.submit}
							>
								ACTUALIZAR
							</Button>
						</Grid>
					</Grid>
				</form>
			</div>
		</LayoutDashboard>
	);
}