import React, { useContext, useEffect } from 'react';
import LayoutDashboard from '../../Components/Layout/LayoutDashboard';
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles'; 
import Container  from '@material-ui/core/Container';
import AuthContext from '../../Context/autenticacion/authContext';

const useStyles = makeStyles((theme) => ({

  root: {
    display: 'flex',
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingLeft: theme.spacing(18),
    paddingBottom: theme.spacing(4),
    paddingInlineEnd: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginBlockEnd: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  input: {
    display: 'none',
  },
}));

const Home = props => {
  const classes = useStyles();
  
  //Extraer la informacion de autenticación
  const authContext = useContext(AuthContext);
  const { usuarioAutenticado } = authContext;
  
  useEffect(() => {
    usuarioAutenticado();
  }, []);

  return (
    <LayoutDashboard>
    <div className={classes.root}>
      <CssBaseline />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container className={classes.container}>
          <Grid Container spacing={3}>
            <Typography component="h1" variant="h4" align="center">
              Principal
            </Typography>
          </Grid>
      </Container>
    </main>
  </div > 
  </LayoutDashboard>
);
}

export default Home;