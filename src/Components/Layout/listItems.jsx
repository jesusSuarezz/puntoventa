import React from 'react';
import {
     ListItem, ListItemIcon, ListItemText, Divider, List
  } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';

import PersonIcon from '@material-ui/icons/Person'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import HomeIcon from '@material-ui/icons/Home';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import StorefrontIcon from '@material-ui/icons/Storefront';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EventIcon from '@material-ui/icons/Event';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import ClassIcon from '@material-ui/icons/Class';
import GradeIcon from '@material-ui/icons/Grade';
import AssignmentIcon from '@material-ui/icons/Assignment';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import DevicesIcon from '@material-ui/icons/Devices';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';


const useStyles = makeStyles((theme) => ({
  root : {
    width : '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  Link: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
}));

export default function  ListItems() {

  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
  <div>
    <List component ="nav">
      <Link to="/Home" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <HomeIcon/>
          </ListItemIcon>
            <ListItemText primary="Inicio"/>
        </ListItem>
      </Link>
      <Divider/>
      <Link to="/Citas" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <EventIcon/>
          </ListItemIcon>
          <ListItemText primary="Agendar Citas"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Clientes" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <PeopleAltIcon/>
          </ListItemIcon>
          <ListItemText primary="Clientes"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Cursos" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <ClassIcon/>
          </ListItemIcon>
          <ListItemText primary="Cursos"/>
        </ListItem>
      </Link>
      <Divider />     
      <Link to="/Diplomados" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <GradeIcon/>
          </ListItemIcon>
          <ListItemText primary="Diplomados"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Metodos de Pago" className={classes.Link}>
        <ListItem button>
          <ListItemIcon> 
            <CreditCardIcon/>
          </ListItemIcon>
          <ListItemText primary="Metodos de Pago"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Pagos" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <MonetizationOnIcon/>
          </ListItemIcon>
          <ListItemText primary="Pagos"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="Productos" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <ShoppingCartIcon/>
          </ListItemIcon>
            <ListItemText primary="Productos"/>
        </ListItem>
      </Link>
      <Divider/>
      <Link to="/Calendarios" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <DateRangeIcon/>
          </ListItemIcon>
          <ListItemText primary="Programar Cursos"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Reglas de Negocio" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <LocalAtmIcon/>
          </ListItemIcon>
          <ListItemText primary="Reglas de negocio"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Salones"  className={classes.Link} >
        <ListItem button>
          <ListItemIcon>
            <HomeWorkIcon/>
          </ListItemIcon>
            <ListItemText primary="Salones"/>
        </ListItem>
      </Link>
      <Divider/>
      <Link to="/Servicios" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <DevicesIcon/>
          </ListItemIcon>
          <ListItemText primary="Servicios"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Sucursales" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <StorefrontIcon/>
          </ListItemIcon>
          <ListItemText primary="Sucursales"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Stock de Productos" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <FormatColorFillIcon/>
          </ListItemIcon>
            <ListItemText primary="Stock de Productos"/>
        </ListItem>
      </Link>
      <Divider/>
      <Link to="/Usuarios" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
            <PersonIcon/>
          </ListItemIcon>
          <ListItemText primary="Usuarios"/>
        </ListItem>
      </Link>
      <Divider />
      <Link to="/Ventas" className={classes.Link}>
        <ListItem button>
          <ListItemIcon>
          <AttachMoneyIcon />
          </ListItemIcon>
          <ListItemText primary="Ventas"/>
        </ListItem>
      </Link>
      </List>
  </div>
  );
}