import React, { useReducer } from 'react';
import AuthContext from './authContext';
import AuthReducer from './authReducer';
import clienteAxios from '../../Config/Axios';
/**Importar Sweetalert2 * */
import Swal from 'sweetalert2';
/**Importar componente token headers */
import tokenAuth from '../../Config/tokenAuth';

import {
	OBTENER_USUARIO,
	LOGIN_EXITOSO,
	LOGIN_ERROR,
	CERRAR_SESION,
} from '../../types/index';


const AuthState = (props) => {
	//Agregar state inicial
	const initialState = {
		token: localStorage.getItem('token'),
		autenticado: null,
		usuario: null,
		cargando: true,
	};

	const [state, dispatch] = useReducer(AuthReducer, initialState);
	//funciones
	//Retorna el usuario autenticado
	const usuarioAutenticado = async () => {
		const token = localStorage.getItem('token');
		console.log('hola'+token)
		if (token) {
			//Todo: Funcion para enviar el token por headers
			tokenAuth(token);
		}
		try {
			 const res = await clienteAxios.get('/users');
			// console.log(res); */
			dispatch({
				type: OBTENER_USUARIO,
				payload: res.data.data,
			});
		} catch (error) {
			console.log(error);
			dispatch({
				type: LOGIN_ERROR,
			});
		}
	};

	//cuando el usuario inicia sesion
	const iniciarSesion = async (datos) => {
		try {
			console.log(datos);
			const res = await clienteAxios.post('/login', datos);
			console.log(res);
			dispatch({
				type: LOGIN_EXITOSO,
				payload: res.data.token,
			});
			usuarioAutenticado();
		} catch (error) {
			Swal.fire('Inició de Sesión Fallido', error.res.data.success, 'error');
		}
	};

	//Cierrra sesion del usuario
	const cerrarSesion = () => {
		dispatch({
			type: CERRAR_SESION,
		});
	};
	return (
		<AuthContext.Provider
			value={{
				token: state.token,
				autenticado: state.autenticado,
				usuario: state.usuario,
				cargando: state.cargando,
				iniciarSesion,
				usuarioAutenticado,
				cerrarSesion,
			}}
		>
			{props.children}
		</AuthContext.Provider>
	);
};

export default AuthState;
