import {
	REGISTRO_EXITOSO,
	OBTENER_USUARIO,
	LOGIN_EXITOSO,
	CERRAR_SESION,
	LOGIN_ERROR,
} from '../../types/index';

export default (state, action) => {
	switch (action.type) {
		case REGISTRO_EXITOSO:
			return {
				...state,
			};
		case LOGIN_EXITOSO:
			localStorage.setItem('token', action.payload);
			return {
				...state,
				autenticado: true,
				cargando: false,
			};
		case OBTENER_USUARIO:
			return {
				...state,
				autenticado: true,
				usuario: action.payload,
				cargando: false,
			};
		case LOGIN_ERROR:
		case CERRAR_SESION:
			localStorage.removeItem('token');
			return {
				...state,
				token: null,
				usuario: null,
				autenticado: null,
				cargando: false,
			};
		default:
			return state;
	}
};
