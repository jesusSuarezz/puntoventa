import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from '../src/Routers/App';
import * as serviceWorker from './serviceWorker';

//const cors = require('cors') // ocupamos cors para obtener peticiones de distintos servidores

//habilitar cors
//app.use(cors())

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
