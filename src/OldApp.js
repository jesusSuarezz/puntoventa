import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import Category from './Components/Containers/Category';
import Login from './Components/Login';
import LayoutDashboard from './Components/Layout/LayoutDashboard';
import ClientIndex from '../Containers/Clients/Index';



const App = () => (

  <BrowserRouter>
    <LayoutDashboard>
      <Switch>
        <Route exact path="/Clientes" component={ClientIndex} />
      </Switch>
    </LayoutDashboard>
  </BrowserRouter>

)

export default App;  

