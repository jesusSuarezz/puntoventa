import React from 'react';
import { Route , BrowserRouter as Router,  Switch } from 'react-router-dom';

//import Login from './Components/Forms/Auth/Login';
import Home from '../Containers/Principal/Home';
 
import BusinessRulesAdd from '../Containers/Business_Rules/Add';
import BusinessRulesEdit from '../Containers/Business_Rules/Edit';
import BusinessRulesIndex from '../Containers/Business_Rules/Index';

import CoursesIndex from '../Containers/Courses/Index';
import CoursesEdit from '../Containers/Courses/Edit';
import CoursesAdd from '../Containers/Courses/Add';

import ClientIndex from '../Containers/Clients/Index';
import ClientAdd from '../Containers/Clients/Add';
import ClientEdit  from '../Containers/Clients/Edit';

import ClassroomsAdd from '../Containers/Classrooms/Add';
import ClassroomsEdit from '../Containers/Classrooms/Edit';
import ClassroomsIndex from '../Containers/Classrooms/Index';

import ScheduleAdd from '../Containers/Schedule/Add';
import ScheduleEdit from '../Containers/Schedule/Edit';
import ScheduleIndex from '../Containers/Schedule/Index';

import PaymentMethodsIndex from '../Containers/Payments_Methods/Index';
import PaymentMethodsEdit from '../Containers/Payments_Methods/Edit';
import PaymentMethodsAdd from '../Containers/Payments_Methods/Add';

import ProductStockAdd from '../Containers/Products_Stocks/Add';
import ProductStockEdit from '../Containers/Products_Stocks/Edit';
import ProductStockIndex from '../Containers/Products_Stocks/Index';

import ProductAdd from '../Containers/Products/Add';
import ProductEdit from '../Containers/Products/Edit';
import ProductIndex from '../Containers/Products/Index';

import PaymentsIndex from '../Containers/Payments/Index';
import PaymentsEdit from '../Containers/Payments/Edit';
import PaymentsAdd from '../Containers/Payments/Add';

//INDEX PARA LOS TABS
import SalesIndex from '../Containers/Sales/Index';
//IMPORTAMOS COMPONENTES DE PRODUCTOS
import SalesAddProduct from '../Containers/Sales/Add_SaleProduct';
import SalesEditProduct from '../Containers/Sales/Edit_SaleProduct';
//IMPORTAMOS COMPONENTES DE APLICACION DE UÑAS
import SalesAddApplication from '../Containers/Sales/Add_SaleApplication';
import SalesEditApplication from '../Containers/Sales/Edit_SaleApplication';
//IMPORTAMOS COMPONENTES DE INSCRIPCIONES
import InscriptionAdd from '../Containers/Inscriptions/Add_SaleInscription';
import InscriptionEdit from '../Containers/Inscriptions/Edit_SaleInscription';

import DetailsInscriptions from '../Containers/Sales_Details/Inscriptions';
import DetailsSales from '../Containers/Sales_Details/SalesProducts';
import DetailsAplication from '../Containers/Sales_Details/Aplication';

import UserIndex from '../Containers/Users/Index';
import UserEdit from '../Containers/Users/Edit';
import UserAdd from '../Containers/Users/Add';

import OfficeIndex from '../Containers/Branch_Offices/Index';
import OfficeAdd from '../Containers/Branch_Offices/Add';
import OfficeEdit from '../Containers/Branch_Offices/Edit';

import GraduateIndex from '../Containers/Graduates/Index';
import GraduateAdd from '../Containers/Graduates/Add';
import GraduateEdit from '../Containers/Graduates/Edit';

import ServiceIndex from '../Containers/Services/Index';
import ServiceAdd from '../Containers/Services/Add';
import ServiceEdit from '../Containers/Services/Edit';

import ServiceScheduleIndex from '../Containers/Service_Schedules/Index';
import ServiceScheduleAdd from '../Containers/Service_Schedules/Add';
import ServiceScheduleEdit from '../Containers/Service_Schedules/Edit';

/* Componente para el registros de un usuario */
import SignInSide from '../Components/Auth/Register';

/* Componente de Login */
import Login from '../Components/Auth/Login';


/*  Import componentente PasswordReset */
import PasswordReset from '../Components/PasswordReset/PasswordReset';

/**Importar el authState */
import AuthState from '../Context/autenticacion/authState';

/**Importar token header axios */
import tokenAuth  from '../Config/tokenAuth';

import RutaPrivada from './RoutePrivate';

//Revisar si tenemos algun token
const token = localStorage.getItem('token');
if(token){
  tokenAuth(token);
}




function App () {
    //console.log(process.env.REACT_APP_BACKENTD_URL); //revisamos el entorno de la api
 

  return (
		<AuthState>
			<Router>
				<Switch>
					{/**RUTA PARA PAGINA PRINCIPAL */}
					<RutaPrivada exact path="/Home" component={Home} />
					{/*RUTA USERS*/}
					<RutaPrivada exact path="/Usuarios" component={UserIndex} />
					<RutaPrivada exact path="/Agregar_Usuario" component={UserAdd} />
					<RutaPrivada exact path="/Editar_Usuario/:id" component={UserEdit} />
					{/**RUTA COURSES */}
					<RutaPrivada exact path="/Cursos" component={CoursesIndex} />
					<RutaPrivada exact path="/Agregar_Curso" component={CoursesAdd} />
					<RutaPrivada exact path="/Editar_Curso/:id" component={CoursesEdit} />
					{/**RUTA CLASSROOMS */}
					<RutaPrivada exact path="/Salones" component={ClassroomsIndex} />
					<RutaPrivada exact path="/Agregar_Salon" component={ClassroomsAdd} />
					<RutaPrivada exact path="/Editar_Salon/:id" component={ClassroomsEdit} />
					{/**RUTA BRANCH_OFFICES */}
					<RutaPrivada exact path="/Sucursales" component={OfficeIndex} />
					<RutaPrivada exact path="/Agregar_Sucursal" component={OfficeAdd} />
					<RutaPrivada exact path="/Editar_Sucursal/:id" component={OfficeEdit} />
					{/**RUTA GRADUATES */}
					<RutaPrivada exact path="/Diplomados" component={GraduateIndex} />
					<RutaPrivada exact path="/Agregar_Diplomado" component={GraduateAdd} />
					<RutaPrivada exact path="/Editar_Diplomado/:id" component={GraduateEdit} />
					{/**RUTAS Schedule */}
					<RutaPrivada exact path="/Calendarios" component={ScheduleIndex} />
					<RutaPrivada exact path="/Agregar_Calendario" component={ScheduleAdd} />
					<RutaPrivada exact path="/Editar_Calendario/:id" component={ScheduleEdit} />
					{/**RUTA CLIENTS */}
					<RutaPrivada exact path="/Clientes" component={ClientIndex} />
					<RutaPrivada exact path="/Agregar_Cliente" component={ClientAdd} />
					<RutaPrivada exact path="/Editar_Cliente/:id" component={ClientEdit} />
					{/*RUTA REGLAS DE NEGOCIOS */}
					<RutaPrivada
						exact
						path="/Reglas de Negocio"
						component={BusinessRulesIndex}
					/>
					<RutaPrivada exact path="/Agregar_Regla" component={BusinessRulesAdd} />
					<RutaPrivada exact path="/Editar_Regla/:id" component={BusinessRulesEdit} />
					{/**RUTA PAYMENTS */}
					<RutaPrivada exact path="/Agregar_Pago" component={PaymentsAdd} />
					<RutaPrivada exact path="/Editar_Pago/:id" component={PaymentsEdit} />
					<RutaPrivada exact path="/Pagos" component={PaymentsIndex} />
					{/*RUTA PAYMENTS_METHODS*/}
					<RutaPrivada
						exact
						path="/Metodos de Pago"
						component={PaymentMethodsIndex}
					/>
					<RutaPrivada exact path="/Agregar_Metodo" component={PaymentMethodsAdd} />
					<RutaPrivada
						exact
						path="/Editar_Metodo/:id"
						component={PaymentMethodsEdit}
					/>
					{/**RUTAS PRODUCTS */}
					<RutaPrivada exact path="/Agregar_Producto" component={ProductAdd} />
					<RutaPrivada exact path="/Editar_Producto/:id" component={ProductEdit} />
					<RutaPrivada exact path="/Productos" component={ProductIndex} />
					{/*RUTAS PRODUCTS_STOCK*/}
					<RutaPrivada
						exact
						path="/Stock de Productos"
						component={ProductStockIndex}
					/>
					<RutaPrivada exact path="/Agregar_Stock" component={ProductStockAdd} />
					<RutaPrivada exact path="/Editar_Stock/:id" component={ProductStockEdit} />
					{/**RUTAS SALES_DETAILS */}
					<RutaPrivada
						exact
						path="/Detalle_Inscripcion"
						component={DetailsInscriptions}
					/>
					<RutaPrivada exact path="/Detalle_Tienda" component={DetailsSales} />
					<RutaPrivada
						exact
						path="/Detalle_AplicacionUñas"
						component={DetailsAplication}
					/>
					{/**RUTAS SALES PRODUCTOS Y APLICACIONES */}
					<RutaPrivada exact path="/Ventas" component={SalesIndex} />
					<RutaPrivada
						exact
						path="/Agregar_VentaProducto"
						component={SalesAddProduct}
					/>
					<RutaPrivada
						exact
						path="/Editar_VentaProducto/:id"
						component={SalesEditProduct}
					/>
					<RutaPrivada
						exact
						path="/Agregar_VentaAplicacion"
						component={SalesAddApplication}
					/>
					<RutaPrivada
						exact
						path="/Editar_VentaAplicacion/:id"
						component={SalesEditApplication}
					/>
					{/**RUTA INSCRIPTIONS */}
					<RutaPrivada
						exact
						path="/Agregar_VentaInscripcion"
						component={InscriptionAdd}
					/>
					<RutaPrivada
						exact
						path="/Editar_VentaInscripcion/:id"
						component={InscriptionEdit}
					/>
					{/*RUTA SERVICE_SCHEDULES */}
					<RutaPrivada exact path="/Citas" component={ServiceScheduleIndex} />
					<RutaPrivada
						exact
						path="/Agregar_Cita"
						component={ServiceScheduleAdd}
					/>
					<RutaPrivada
						exact
						path="/Editar_Cita/:id"
						component={ServiceScheduleEdit}
					/>
					{/*RUTA SERVICES */}
					<RutaPrivada exact path="/Servicios" component={ServiceIndex} />
					<RutaPrivada exact path="/Agregar_Servicio" component={ServiceAdd} />
					<RutaPrivada exact path="/Editar_Servicio/:id" component={ServiceEdit} />

					{/**Ruta Login */}
					<Route exact path="/" component={Login}></Route>
					{/**Ruta Recuperar password */}
					<Route exact path="/password-reset" component={PasswordReset}></Route>

					<Route exact path="/Registro" component={SignInSide} />
				</Switch>
			</Router>
		</AuthState>
	);}

export default App;
