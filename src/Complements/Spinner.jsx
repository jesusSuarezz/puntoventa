import React from 'react';
import './Spinner.css';

function Spinner() {
	return (
		<div class="div_contenedor">
			<div class="div_centrado">
				<div class="preloader"></div>
			</div>
		</div>
	);
}
export default Spinner;
