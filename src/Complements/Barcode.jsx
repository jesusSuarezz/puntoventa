import React from 'react';
/**Importar generador de codigo de barras */
import { useBarcode } from '@createnextapp/react-barcode';

function BarCode({ textobarcode }) {
	const { inputRef } = useBarcode({
		value: textobarcode == '' ? 'vacio' : textobarcode,
		options: {
			background: '#FFFFFF',
		},
	});

	return <img ref={inputRef} />;
}

export default BarCode;
